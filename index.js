const Converter = require("csvtojson").Converter;
const fs = require('fs');
const path = require('path');
const program = require('commander');

var converter = new Converter({});
var dataDir;

program
    .version(require('./package.json').version)
    .usage('<outputDir>')
    .arguments('<outputDir>')
    .action(outputDir => {
      dataDir = outputDir;
    })
    .parse(process.argv);

if (typeof dataDir === 'undefined') {
  console.error('No output directory given.');
  process.exit(1);
}

fs.createReadStream("./source.csv").pipe(converter);

converter.on('end_parsed', jsonArray => {
  jsonArray.forEach(program => {
    fs.writeFile(
      path.join(
        dataDir,
        `${program.Name.toLowerCase().replace(/\s/g, '_')}.yaml`
      ),
      require('js-yaml').safeDump(program)
    );
  });
});
